--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: doctor; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.doctor (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    username character varying(255),
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL
);


ALTER TABLE public.doctor OWNER TO root;

--
-- Name: patient; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.patient (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE public.patient OWNER TO root;

--
-- Name: time_slot; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.time_slot (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL,
    doctor_id uuid NOT NULL,
    patient_id uuid NOT NULL
);


ALTER TABLE public.time_slot OWNER TO root;

--
-- Name: user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public."user" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    role_name character varying(255)
);


ALTER TABLE public."user" OWNER TO root;

--
-- Data for Name: doctor; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Data for Name: patient; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Data for Name: time_slot; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Name: doctor doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.doctor
    ADD CONSTRAINT doctor_pkey PRIMARY KEY (id);


--
-- Name: patient patient_pk; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT patient_pk PRIMARY KEY (id);


--
-- Name: time_slot time_slot_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.time_slot
    ADD CONSTRAINT time_slot_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: patient_user_id_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX patient_user_id_uindex ON public.patient USING btree (username);


--
-- Name: user_username_uindex; Type: INDEX; Schema: public; Owner: root
--

CREATE UNIQUE INDEX user_username_uindex ON public."user" USING btree (username);


--
-- Name: time_slot time_slot_doctor_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.time_slot
    ADD CONSTRAINT time_slot_doctor_id_fk FOREIGN KEY (doctor_id) REFERENCES public.doctor(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: time_slot time_slot_patient_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.time_slot
    ADD CONSTRAINT time_slot_patient_id_fk FOREIGN KEY (patient_id) REFERENCES public.patient(id) ON UPDATE CASCADE ON DELETE CASCADE;

INSERT INTO public.doctor (id, first_name, last_name, username, start_time, end_time) VALUES ('28f7cf9c-16fa-47d8-872c-66ab25fb8fbc', 'Jasmine', 'Row', 'doctor1', '09:00:00', '18:00:00');
INSERT INTO public.doctor (id, first_name, last_name, username, start_time, end_time) VALUES ('1f765fb6-2be3-43f1-8cf5-670902e87a05', 'Jenny', 'Harrison', 'doctor3', '12:00:00', '18:00:00');

INSERT INTO public.patient (id, first_name, last_name, username) VALUES ('9689c48a-1261-4f6b-a0c7-32c29adb558d', 'William', 'patient', 'pat');

INSERT INTO public."user" (id, username, password, role_name) VALUES ('8ea0a27a-519a-4263-8fd7-bf7c565096a8', 'admin', 'password', 'ADMIN');
INSERT INTO public."user" (id, username, password, role_name) VALUES ('c9a11461-6089-438b-87c6-c9b13aa84003', 'patient', 'password', 'PATIENT');
INSERT INTO public."user" (id, username, password, role_name) VALUES ('11998b69-9849-45db-8225-e61125aa9968', 'doctor1', 'password', 'DOCTOR');
INSERT INTO public."user" (id, username, password, role_name) VALUES ('4e4627d3-88e8-4b18-810b-ec27ad3d373d', 'doctor2', 'password', 'DOCTOR');
INSERT INTO public."user" (id, username, password, role_name) VALUES ('4ebcafd4-eb1a-4ec4-9619-ef32a1e3b9f0', 'doctor3', 'password', 'DOCTOR');

INSERT INTO public.time_slot (id, start_time, end_time, doctor_id, patient_id) VALUES ('b738b439-c36f-4dbe-bd52-588e1686b1eb', '12:00:00', '13:00:00', '28f7cf9c-16fa-47d8-872c-66ab25fb8fbc', '9689c48a-1261-4f6b-a0c7-32c29adb558d');
INSERT INTO public.time_slot (id, start_time, end_time, doctor_id, patient_id) VALUES ('a2413887-1c28-4bd8-8e16-22bd70b249ba', '09:00:00', '10:00:00', '28f7cf9c-16fa-47d8-872c-66ab25fb8fbc', '9689c48a-1261-4f6b-a0c7-32c29adb558d');

--
-- PostgreSQL database dump complete
--

