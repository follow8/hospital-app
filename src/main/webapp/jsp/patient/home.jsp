<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Patient</title>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="admin">Doctor Service</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/patient">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="patient/timeSlots">Booked time</a>
            </li>
        </ul>
    </div>
</nav>
<main role="main">
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <c:choose>
                    <c:when test="${not empty doctorList}">
                        <c:forEach var="doctor" items="${doctorList}">
                            <div class="col-md-4 mt-4">
                                <div class="card mb-4 shadow-sm">
                                    <div class="card-body">
                                        <p class="card-text">Doctor: ${doctor.firstName} ${doctor.lastName}</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <a href="${pageContext.request.contextPath}/patient/doctorSchedule?doctorId=${doctor.id}"
                                                   class="btn btn-sm btn-primary">View schedule</a>
                                                <a href="${pageContext.request.contextPath}/patient/bookTimeSlot?doctorId=${doctor.id}"
                                                   class="btn btn-sm btn-primary">Book time slot</a>
                                            </div>
                                            <small class="text-muted">9 mins</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <br>
                        <div class="alert alert-info">
                            No doctors found
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</main>
</body>
</html>

