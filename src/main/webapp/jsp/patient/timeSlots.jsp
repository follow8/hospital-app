<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Patient</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${not empty timeSlotList}">
            <h3 class="text-center">Current time slots</h3>
            <ul class="list-group">
                <c:forEach var="timeSlot" items="${timeSlotList}">
                    <li class="list-group-item">
                        <div class="d-flex justify-content-between">
                            <h5><span class="p-2">${timeSlot.startTime} - ${timeSlot.endTime}</span></h5>
                        </div>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <h3>You haven't booked any time slots yet!</h3>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
