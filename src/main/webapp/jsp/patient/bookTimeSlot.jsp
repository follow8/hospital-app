<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Patient</title>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="d-flex flex-row justify-content-between">
        <div class="mt-4 mb-4 badge badge-primary" style="font-size: 24px;">Work start time: ${doctor.workTime.startTime}</div>
        <div class="mt-4 mb-4 badge badge-primary" style="font-size: 24px;">Work end time: ${doctor.workTime.endTime}</div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <form action="${pageContext.request.contextPath}/patient/bookTimeSlot" method="post">
                <div class="form-group">
                    <div class="input-group date mt-2 mb-2" id="datetimepicker3" data-target-input="nearest">
                        <input type="text" name="startTime" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                        <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                    <div class="input-group date mt-2 mb-2" id="datetimepicker4" data-target-input="nearest">
                        <input type="text" name="endTime" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                        <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                    <input type="hidden" name="doctorId" value="${doctor.id}">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Book time slot</button>
                </div>
            </form>
        </div>
    </div>
    <c:if test="${timeSlotIsBusyMessage != null}">
        <h3>${timeSlotIsBusyMessage}</h3>
    </c:if>
    <c:if test="${incorrectTime != null}">
        <h3>${incorrectTime}</h3>
    </c:if>
</div>
<div class="container">
    <c:choose>
        <c:when test="${timeSlotIsBusyMessage == null}">
            <h3>Available time:</h3>
        </c:when>
        <c:otherwise>
            <h3>Nearest available time:</h3>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty timeSlotList && timeSlotList != null}">
            <ul class="list-group">
                <c:forEach var="timeSlot" items="${timeSlotList}">
                    <li class="list-group-item">
                        <div class="d-flex justify-content-between">
                            <h5 class="mb-1">${timeSlot.patient.firstName} ${timeSlot.patient.lastName}</h5>
                            <h5><span class="p-2">${timeSlot.startTime} - ${timeSlot.endTime}</span></h5>
                        </div>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <h3>No available time</h3>
        </c:otherwise>
    </c:choose>
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker3').datetimepicker({
            format: 'HH:mm',
        });
        $('#datetimepicker4').datetimepicker({
            format: 'HH:mm'
        });
    });
</script>
</body>
</html>
