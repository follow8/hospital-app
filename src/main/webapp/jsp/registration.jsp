<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <title>Sign In</title>
    <link href="http://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/docs/4.1/examples/sign-in/signin.css" rel="stylesheet">
</head>

<body class="text-center">
<form action="registration" method="post" class="form-signin">
    <img class="mb-4" src="http://getbootstrap.com/docs/4.1/assets/brand/bootstrap-solid.svg" alt="" width="72"
         height="72">
    <h1 class="h3 mb-3 font-weight-normal">Registration</h1>
    <label for="input-first-name" class="sr-only">First name</label>
    <input type="text" name="firstName" id="input-first-name" class="form-control" placeholder="First name" required autofocus>

    <label for="input-last-name" class="sr-only">Last name</label>
    <input type="text" name="lastName" id="input-last-name" class="form-control" placeholder="Last name" required autofocus>

    <label for="input-username" class="sr-only">Username</label>
    <input type="text" name="username" id="input-username" class="form-control" placeholder="Username" required autofocus>

    <label for="input-password" class="sr-only">Password</label>
    <input type="password" name="password" id="input-password" class="form-control" placeholder="Password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    <a href="authorization" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    <c:if test="${errorMessage != null}">
        <h3>${errorMessage}</h3>
    </c:if>
</form>
</body>
</html>
