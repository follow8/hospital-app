<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Doctor</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <c:set var="timeSlotList" value="${doctor.timeSlots}"></c:set>
    <div class="d-flex flex-row justify-content-between">
        <div class="mt-4 mb-4 badge badge-primary" style="font-size: 24px;">Work start time: ${doctor.workTime.startTime}</div>
        <div class="mt-4 mb-4 badge badge-primary" style="font-size: 24px;">Work end time: ${doctor.workTime.endTime}</div>
    </div>
    <c:choose>
        <c:when test="${not empty timeSlotList && timeSlotList != null}">
            <ul class="list-group">
                <c:forEach var="timeSlot" items="${timeSlotList}">
                    <li class="list-group-item">
                        <div class="d-flex justify-content-between">
                            <h5 class="mb-1">${timeSlot.patient.firstName} ${timeSlot.patient.lastName}</h5>
                            <h5><span class="p-2">${timeSlot.startTime} - ${timeSlot.endTime}</span></h5>
                        </div>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <br>
            <div class="alert alert-info">
                You have no patients currently!
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
