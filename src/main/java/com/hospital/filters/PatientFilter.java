package com.hospital.filters;

import com.hospital.model.Role;
import com.hospital.model.User;
import com.hospital.services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/patient/*")
public class PatientFilter implements Filter {
    private UserService userService = new UserService();
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        String username = ((String) req.getSession().getAttribute("username"));
        User user = userService.findByUsername(username);
        if(user != null && user.getRole().equals(Role.PATIENT)) {
            chain.doFilter(request, response);
        }else {
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
