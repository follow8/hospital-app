package com.hospital.dao;


import com.hospital.DbManager;
import com.hospital.exceptions.UserNotFoundException;
import com.hospital.model.Role;
import com.hospital.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class UserDAO {
    private static final Logger logger = LogManager.getLogger(DoctorDAO.class);
    private static final String INSERT = "insert into \"user\" (username, password, role_name) values (?,?,?);";
    private static final String UPDATE_BY_ID = "update \"user\" set  username = ?, password = ?, role_name = ? where id = ?;";
    private static final String SELECT_BY_ID = "select * from \"user\" where id = ?;";
    private static final String SELECT_BY_USERNAME = "select * from \"user\" where username = ?;";
    private static final String SELECT_ALL = "select * from \"user\";";
    private static final String DELETE_BY_USERNAME = "delete from \"user\" where username = ?;";
    private static final String DELETE_BY_ID = "delete from \"user\" where id = ?;";


    public UserDAO() {

    }

    public void save(User user) {
        try (Connection con = DbManager.getConnection()) {
            if (user.getId() == null) {
                try (PreparedStatement st = con.prepareStatement(INSERT)) {
                    st.setString(1, user.getUsername());
                    st.setString(2, user.getPassword());
                    st.setString(3, user.getRole().name());
                    st.executeUpdate();
                }
            } else {
                try (PreparedStatement st = con.prepareStatement(UPDATE_BY_ID)) {
                    st.setString(1, user.getUsername());
                    st.setString(2, user.getPassword());
                    st.setString(3, user.getRole().name());
                    st.setObject(4, user.getId());
                    st.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }
    }


    public User findById(UUID id) {
        User user = null;
        if (id != null) {
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(SELECT_BY_ID)) {
                st.setObject(1, id);
                try (ResultSet res = st.executeQuery()) {
                    user = createNewInstance(res);
                }
            } catch (SQLException | ClassCastException ex) {
                logger.error(ex.getMessage());
            }
        }

        return user;
    }

    public User createNewInstance(ResultSet res) throws SQLException {
        User user = null;
        if (res.next()) {
            user = new User();
            user.setId((UUID) res.getObject("id"));
            user.setUsername(res.getString("username"));
            user.setPassword(res.getString("password"));
            user.setRole(Role.valueOf(res.getString("role_name")));
        }
        return user;
    }

    public User findByUsername(String username) {
        User user = null;
        if (username != null && username.isEmpty() == false) {
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(SELECT_BY_USERNAME)) {
                st.setString(1, username);
                try (ResultSet res = st.executeQuery()) {
                    user = createNewInstance(res);
                }
            } catch (SQLException | ClassCastException ex) {
                ex.printStackTrace();
            }
        }

        return user;
    }

    public Set<User> findAll() {
        Set<User> users = new HashSet<>();
        try(Connection con = DbManager.getConnection();
            PreparedStatement st = con.prepareStatement(SELECT_ALL);
            ResultSet res = st.executeQuery()) {
            boolean flag = true;
            while(res.next()) {
                User user = createNewInstance(res);
                if(user != null) {
                    users.add(user);
                }else {
                    flag = false;
                }
            }
        }catch (SQLException | ClassCastException ex) {
            logger.error(ex.getMessage());
        }

        return users;
    }

    public void deleteByUsername(String username) throws UserNotFoundException {
        if(username != null && username.isEmpty() == false) {
            try(Connection con = DbManager.getConnection();
                PreparedStatement st = con.prepareStatement(DELETE_BY_USERNAME)) {
                st.setString(1, username);
                st.executeUpdate();
            }catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }else {
            throw new UserNotFoundException();
        }
    }

    public void deleteById(UUID id) throws UserNotFoundException {
        if(id != null) {
            try(Connection con = DbManager.getConnection();
                PreparedStatement st = con.prepareStatement(DELETE_BY_ID)) {
                st.setObject(1, id);
                st.executeUpdate();
            }catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }else {
            throw new UserNotFoundException();
        }
    }
}
