package com.hospital.dao;

import com.hospital.DbManager;
import com.hospital.exceptions.PatientNotFoundException;
import com.hospital.model.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class PatientDAO {
    private static final Logger logger = LogManager.getLogger(DoctorDAO.class);
    private static final String INSERT = "insert into patient(first_name, last_name, username) values (?,?,?);";
    private static final String UPDATE_BY_ID = "update patient set first_name = ?, last_name = ?, username = ? where id = ?;";
    private static final String SELECT_WHERE = "select * from patient where %s;";
    private static final String SELECT_ALL = "select * from patient;";
    private static final String DELETE_BY_ID = "delete from patient where id = ?;";

    public PatientDAO() {

    }

    public void save(Patient patient) {
        try (Connection con = DbManager.getConnection()) {
            if (patient.getId() == null) {
                try (PreparedStatement st = con.prepareStatement(INSERT)) {
                    st.setString(1, patient.getFirstName());
                    st.setString(2, patient.getLastName());
                    st.setString(3, patient.getUsername());
                    st.executeUpdate();
                }
            } else {
                try (PreparedStatement st = con.prepareStatement(UPDATE_BY_ID)) {
                    st.setString(1, patient.getFirstName());
                    st.setString(2, patient.getLastName());
                    st.setString(3, patient.getUsername());
                    st.setObject(4, patient.getId());
                    st.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }
    }


    public Patient findById(UUID id) {
        Patient patient = null;
        if (id != null) {
            String query = String.format(SELECT_WHERE, "id = ?");
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(query)) {
                st.setObject(1, id);
                try (ResultSet res = st.executeQuery()) {
                    patient = createNewDataObject(res);
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }

        return patient;
    }

    private Patient createNewDataObject(ResultSet res) throws SQLException {
        Patient patient = null;
        if (res.next()) {
            patient = new Patient();
            patient.setId((UUID) res.getObject("id"));
            patient.setFirstName(res.getString("first_name"));
            patient.setLastName(res.getString("last_name"));
            patient.setUsername(res.getString("username"));
        }

        return patient;
    }

    public Patient findByUsername(String username) {
        Patient patient = null;
        if (username != null && username.isEmpty() == false) {
            String query = String.format(SELECT_WHERE, "username = ?");
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(query)) {
                st.setString(1, username);
                try (ResultSet res = st.executeQuery()) {
                    patient = createNewDataObject(res);
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }

        return patient;
    }

    public Set<Patient> findAll() {
        Set<Patient> patients = new HashSet<>();
        try (Connection con = DbManager.getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_ALL);
             ResultSet res = st.executeQuery()) {
            boolean flag = true;
            while (flag) {
                Patient patient = createNewDataObject(res);
                if (patient != null) {
                    patients.add(patient);
                }else {
                    flag = false;
                }
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }

        return patients;
    }

    public void delete(Patient patient) throws PatientNotFoundException {
        if (patient.getId() != null) {
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(DELETE_BY_ID)) {
                st.setObject(1, patient.getId());
                st.executeUpdate();
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        } else {
            throw new PatientNotFoundException();
        }
    }
}
