package com.hospital.dao;

import com.hospital.DbManager;
import com.hospital.exceptions.DoctorNotFoundException;
import com.hospital.model.Doctor;
import com.hospital.model.TimeSlot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;

public class DoctorDAO {
    private static final Logger logger = LogManager.getLogger(DoctorDAO.class);
    private static final String INSERT = "insert into doctor(first_name, last_name, username, start_time, end_time) values (?,?,?,?,?);";
    private static final String UPDATE_BY_ID = "update doctor set first_name = ?, last_name = ?, username = ?, start_time = ?, end_time = ? where id = ?;";
    private static final String SELECT_WHERE = "select * from doctor where %s = ?;";
    private static final String SELECT_ALL = "select * from doctor;";
    private static final String DELETE_BY_ID = "delete from doctor where id = ?;";

    public DoctorDAO() {

    }

    public void save(Doctor doctor) {
        try (Connection con = DbManager.getConnection()) {
            TimeSlot workTime = doctor.getWorkTime();
            if (doctor.getId() == null) {
                try (PreparedStatement st = con.prepareStatement(INSERT)) {
                    st.setString(1, doctor.getFirstName());
                    st.setString(2, doctor.getLastName());
                    st.setString(3, doctor.getUsername());
                    st.setTime(4, Time.valueOf(workTime.getStartTime()));
                    st.setTime(5, Time.valueOf(workTime.getEndTime()));
                    st.executeUpdate();
                }
            } else {
                try (PreparedStatement st = con.prepareStatement(UPDATE_BY_ID)) {
                    st.setString(1, doctor.getFirstName());
                    st.setString(2, doctor.getLastName());
                    st.setString(3, doctor.getUsername());
                    st.setTime(4, Time.valueOf(workTime.getStartTime()));
                    st.setTime(5, Time.valueOf(workTime.getEndTime()));
                    st.setObject(6, doctor.getId());
                    st.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private Doctor createNewDataObject(ResultSet res) throws SQLException {
        Doctor doc = null;
        if (res.next()) {
            doc = new Doctor();
            doc.setId((UUID) res.getObject("id"));
            doc.setFirstName(res.getString("first_name"));
            doc.setLastName(res.getString("last_name"));
            doc.setUsername(res.getString("username"));
            TimeSlot ts = new TimeSlot(res.getTime("start_time").toLocalTime(),
                    res.getTime("end_time").toLocalTime());
            doc.setWorkTime(ts);
        }

        return doc;
    }

    public Doctor findById(UUID id) {
        Doctor doc = null;
        if (id != null) {
            String query = String.format(SELECT_WHERE, "id");
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(query)) {
                st.setObject(1, id);
                try (ResultSet res = st.executeQuery()) {
                    doc = createNewDataObject(res);
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }

        return doc;
    }

    public Doctor findByUsername(String username) {
        Doctor doc = null;
        if (username != null && username.isEmpty() == false) {
            String query = String.format(SELECT_WHERE, "username");
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(query)) {
                st.setString(1, username);
                try (ResultSet res = st.executeQuery()) {
                    doc = createNewDataObject(res);
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }

        return doc;
    }


    public Set<Doctor> findAll() {
        Set<Doctor> doctors = new HashSet<>();
        try (Connection con = DbManager.getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_ALL);
             ResultSet res = st.executeQuery()) {
            boolean flag = true;
            while (flag) {
                Doctor doc = createNewDataObject(res);
                if(doc != null) {
                    doctors.add(doc);
                }else {
                    flag = false;
                }
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }

        return doctors;
    }

    public void deleteById(UUID id) throws DoctorNotFoundException {
        if (id != null) {
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(DELETE_BY_ID)) {
                st.setObject(1, id);
                st.executeUpdate();
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        } else {
            throw new DoctorNotFoundException();
        }
    }
}
