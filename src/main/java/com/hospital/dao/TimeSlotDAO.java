package com.hospital.dao;

import com.hospital.DbManager;
import com.hospital.exceptions.TimeSlotNotFoundException;
import com.hospital.model.Patient;
import com.hospital.model.TimeSlot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class TimeSlotDAO {
    private static final Logger logger = LogManager.getLogger(TimeSlotDAO.class);
    private static final String INSERT = "insert into time_slot(start_time, end_time, doctor_id, patient_id) values (?,?,?,?);";
    private static final String UPDATE_BY_ID = "update time_slot set start_time = ?, end_time = ?, doctor_id = ?, patient_id = ? where id = ?;";
    private static final String SELECT_BY_PATIENT_ID = "select * from time_slot where patient_id = ?;";
    private static final String SELECT_BY_DOCTOR_ID = "select time_slot.*, patient.first_name, patient.last_name, patient.username " +
            "from time_slot " +
            "left join patient on time_slot.patient_id = patient.id where doctor_id = ?;";

    private static final String SELECT_ALL = "select * from time_slot;";
    private static final String DELETE_BY_ID = "delete from time_slot where id = ?;";

    public TimeSlotDAO() {
    }

    public void save(TimeSlot timeSlot) {
        try (Connection con = DbManager.getConnection()) {
            if (timeSlot.getId() == null) {
                try (PreparedStatement st = con.prepareStatement(INSERT)) {
                    st.setTime(1, Time.valueOf(timeSlot.getStartTime()));
                    st.setTime(2, Time.valueOf(timeSlot.getEndTime()));
                    st.setObject(3, timeSlot.getDoctorId());
                    st.setObject(4, timeSlot.getPatient().getId());
                    st.executeUpdate();
                }
            } else {
                try (PreparedStatement st = con.prepareStatement(UPDATE_BY_ID)) {
                    st.setTime(1, Time.valueOf(timeSlot.getStartTime()));
                    st.setTime(2, Time.valueOf(timeSlot.getEndTime()));
                    st.setObject(3, timeSlot.getDoctorId());
                    st.setObject(4, timeSlot.getPatient().getId());
                    st.setObject(5, timeSlot.getId());
                    st.executeUpdate();
                }
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }
    }

    private TimeSlot createNewDataObject(ResultSet res) throws SQLException {
        TimeSlot ts = null;
        if(res.next()) {
            ts = new TimeSlot();
            ts.setId((UUID) res.getObject("id"));
            ts.setStartTime(res.getTime("start_time").toLocalTime());
            ts.setEndTime(res.getTime("end_time").toLocalTime());
            ts.setDoctorId((UUID) res.getObject("doctor_id"));
        }

        return ts;
    }

    public Set<TimeSlot> findByDoctorId(UUID id) {
        Set<TimeSlot> timeSlots = new HashSet<>();
        if (id != null) {
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(SELECT_BY_DOCTOR_ID)) {
                st.setObject(1, id);
                try (ResultSet res = st.executeQuery()) {
                    while (res.next()) {
                        TimeSlot ts = new TimeSlot();
                        ts.setId((UUID) res.getObject("id"));
                        ts.setStartTime(res.getTime("start_time").toLocalTime());
                        ts.setEndTime(res.getTime("end_time").toLocalTime());
                        ts.setDoctorId((UUID) res.getObject("doctor_id"));
                        Patient patient = new Patient();
                        patient.setId(((UUID) res.getObject("patient_id")));
                        patient.setFirstName(res.getString("first_name"));
                        patient.setLastName(res.getString("last_name"));
                        patient.setUsername(res.getString("username"));
                        ts.setPatient(patient);
                        timeSlots.add(ts);
                    }
                }
            } catch (SQLException | ClassCastException ex) {
                logger.error(ex.getMessage());
            }
        }

        return timeSlots;
    }

    public Set<TimeSlot> findByPatientId(UUID id) {
        Set<TimeSlot> timeSlots = new HashSet<>();
        if (id != null) {
            try (Connection con = DbManager.getConnection();
                 PreparedStatement st = con.prepareStatement(SELECT_BY_PATIENT_ID)) {
                st.setObject(1, id);
                try (ResultSet res = st.executeQuery()) {
                    boolean flag = true;
                    while (flag) {
                        TimeSlot ts = createNewDataObject(res);
                        if(ts != null) {
                            timeSlots.add(ts);
                        }else {
                            flag = false;
                        }
                    }
                }
            } catch (SQLException | ClassCastException ex) {
                logger.error(ex.getMessage());
            }
        }

        return timeSlots;
    }


    public Set<TimeSlot> findAll() {
        Set<TimeSlot> timeSlots = new HashSet<>();
        try(Connection con = DbManager.getConnection();
        PreparedStatement st = con.prepareStatement(SELECT_ALL);
        ResultSet res = st.executeQuery()) {
            boolean flag = true;
            while (flag) {
                TimeSlot ts = createNewDataObject(res);
                if(ts != null) {
                    timeSlots.add(ts);
                }else {
                    flag = false;
                }
            }
        }catch (SQLException | ClassCastException ex) {
            logger.error(ex.getMessage());
        }

        return timeSlots;
    }

    public void deleteById(UUID id) throws TimeSlotNotFoundException {
        if(id != null) {
            try(Connection con = DbManager.getConnection();
            PreparedStatement st = con.prepareStatement(DELETE_BY_ID)) {
                st.setObject(1, id);
                st.executeUpdate();
            }catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }else {
            throw new TimeSlotNotFoundException();
        }
    }
}
