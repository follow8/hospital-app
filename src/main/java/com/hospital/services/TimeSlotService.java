package com.hospital.services;

import com.hospital.dao.TimeSlotDAO;
import com.hospital.exceptions.TimeSlotNotFoundException;
import com.hospital.model.TimeSlot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Set;
import java.util.UUID;

public class TimeSlotService {
    private static final Logger logger = LogManager.getLogger(TimeSlotService.class);
    private TimeSlotDAO timeSlotDAO;

    public TimeSlotService() {
        timeSlotDAO = new TimeSlotDAO();
    }


    public Set<TimeSlot> findByDoctorId(UUID id) {
        return timeSlotDAO.findByDoctorId(id);
    }

    public Set<TimeSlot> findByPatientId(UUID id) {
        return timeSlotDAO.findByPatientId(id);
    }

    public void save(TimeSlot timeSlot) {
        timeSlotDAO.save(timeSlot);
    }

    public void deleteById(UUID id) throws TimeSlotNotFoundException {
        timeSlotDAO.deleteById(id);
    }

    public Set<TimeSlot> findAll() {
        return timeSlotDAO.findAll();
    }
}
