package com.hospital.services;

import com.hospital.dao.PatientDAO;
import com.hospital.exceptions.PatientNotFoundException;
import com.hospital.model.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Set;
import java.util.UUID;

public class PatientService {
    private static final Logger logger = LogManager.getLogger(PatientService.class);
    private PatientDAO patientDAO;


    public PatientService() {
        patientDAO = new PatientDAO();
    }

    public Patient findById(UUID id) {
        return patientDAO.findById(id);
    }

    public Patient findByUsername(String username) {
        return patientDAO.findByUsername(username);
    }

    public Set<Patient> findAll() {
        return patientDAO.findAll();
    }

    public void save(Patient patient) {
        patientDAO.save(patient);
    }

    public void delte(Patient patient) throws PatientNotFoundException {
        patientDAO.delete(patient);
    }

}
