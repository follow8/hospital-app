package com.hospital.services;

import com.hospital.dao.DoctorDAO;
import com.hospital.dao.TimeSlotDAO;
import com.hospital.dao.UserDAO;
import com.hospital.dto.NewDoctorDto;
import com.hospital.exceptions.DoctorNotFoundException;
import com.hospital.exceptions.TimeSlotIsBusyException;
import com.hospital.exceptions.UserNotFoundException;
import com.hospital.model.Doctor;
import com.hospital.model.TimeSlot;
import com.hospital.model.User;

import java.time.LocalTime;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

public class DoctorService {
    private DoctorDAO doctorDAO;
    private UserDAO userDAO;
    private TimeSlotDAO timeSlotDAO;

    public DoctorService() {
        doctorDAO = new DoctorDAO();
        timeSlotDAO = new TimeSlotDAO();
        userDAO = new UserDAO();
    }

    public Doctor findById(UUID id) {
        Doctor doctor = doctorDAO.findById(id);
        doctor.getTimeSlots().addAll(timeSlotDAO.findByDoctorId(doctor.getId()));
        return doctor;
    }

    public Doctor findByUsername(String username) {
        Doctor doctor = doctorDAO.findByUsername(username);
        doctor.getTimeSlots().addAll(timeSlotDAO.findByDoctorId(doctor.getId()));
        return doctor;
    }

    public Set<Doctor> findAll() {
        return doctorDAO.findAll();
    }

    public void save(NewDoctorDto doc) {
        User user = new User();
        user.setUsername(doc.getUsername());
        user.setPassword(doc.getPassword());
        user.setRole(doc.getRole());
        userDAO.save(user);
        Doctor doctor = new Doctor();
        doctor.setFirstName(doc.getFirstName());
        doctor.setLastName(doc.getLastName());
        doctor.setWorkTime(doc.getWorkTime());
        doctor.setUsername(doc.getUsername());
        doctorDAO.save(doctor);
    }

    public void deleteById(UUID id) throws DoctorNotFoundException {
        doctorDAO.deleteById(id);
    }

    public SortedSet<TimeSlot> getAllAvailableTimeSlots(Doctor doc) {
        return getAllNearestAvailableTimeSlots(doc, doc.getWorkTime().getStartTime().minusHours(1));
    }

    public SortedSet<TimeSlot> getAllNearestAvailableTimeSlots(Doctor doc, LocalTime startTime) {
        doc = findById(doc.getId());
        TimeSlot workTime = doc.getWorkTime();
        SortedSet<TimeSlot> timeSlots = doc.getTimeSlots();
        SortedSet<TimeSlot> nearestTimeSlots = new TreeSet<>(timeSlots.comparator());
        if (timeSlots.isEmpty()) {
            nearestTimeSlots.add(workTime);
            return nearestTimeSlots;
        }
        if (startTime.compareTo(workTime.getEndTime()) >= 0) {
            return nearestTimeSlots;
        }
        TimeSlot first = timeSlots.first();
        TimeSlot last = timeSlots.last();
        if (TimeSlot.durationBetween(last.getEndTime(), workTime.getEndTime()) > 0) {
            nearestTimeSlots.add(new TimeSlot(last.getEndTime(), workTime.getEndTime()));
            if (startTime.compareTo(last.getEndTime()) >= 0) {
                return nearestTimeSlots;
            }
        }
        if (startTime.isBefore(first.getStartTime())) {
            if (TimeSlot.durationBetween(workTime.getStartTime(), first.getStartTime()) > 0) {
                nearestTimeSlots.add(new TimeSlot
                        (workTime.getStartTime(), first.getStartTime()));
            }
        }
        TimeSlot next = ((TreeSet<TimeSlot>) timeSlots).lower(last);
        while (next != null && startTime.compareTo(last.getStartTime()) < 0) {
            if (TimeSlot.durationBetween(next.getEndTime(), last.getStartTime()) > 0) {
                nearestTimeSlots.add(new TimeSlot(next.getEndTime(), last.getStartTime()));
            }
            last = next;
            next = ((TreeSet<TimeSlot>) timeSlots).lower(last);
        }

        return nearestTimeSlots;
    }

    public void addNewTimeSlot(Doctor doc, TimeSlot timeSlot) throws TimeSlotIsBusyException {
        SortedSet<TimeSlot> timeSlots = doc.getTimeSlots();
        if (isTimeSlotAvailable(doc, timeSlot)) {
            timeSlots.add(timeSlot);
            timeSlotDAO.save(timeSlot);
        } else {
            throw new TimeSlotIsBusyException();
        }
    }

    public boolean isTimeSlotAvailable(Doctor doc, TimeSlot timeSlot) {
        TimeSlot workTime = doc.getWorkTime();
        if (workTime.getEndTime().isBefore(timeSlot.getEndTime())
                || workTime.getStartTime().isAfter(timeSlot.getStartTime())) {
            return false;
        }

        return doc.getTimeSlots().stream()
                .noneMatch(timeSlot::isTimeSlotCrossing);
    }

}
