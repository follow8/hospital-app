package com.hospital.services;

import com.hospital.dao.PatientDAO;
import com.hospital.dao.UserDAO;
import com.hospital.dto.SignInUserDto;
import com.hospital.dto.SignUpUserDto;
import com.hospital.exceptions.UserNotFoundException;
import com.hospital.model.Patient;
import com.hospital.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Set;
import java.util.UUID;

public class UserService {
    private static final Logger logger = LogManager.getLogger(UserService.class);
    private UserDAO userDAO;
    private PatientDAO patientDAO;

    public UserService() {
        userDAO = new UserDAO();
        patientDAO = new PatientDAO();
    }

    public boolean signIn(SignInUserDto userDto) {
        User userToFind = userDAO.findByUsername(userDto.getUsername());
        if(userToFind != null &&
                userToFind.getPassword().equals(userDto.getPassword())) {
            return true;
        }else {
            return false;
        }
    }

    public boolean singUp(SignUpUserDto userDto) {
        User user = findByUsername(userDto.getUsername());
        if(user == null) {
            user = new User();
            user.setUsername(userDto.getUsername());
            user.setPassword(userDto.getPassword());
            user.setRole(userDto.getRole());
            userDAO.save(user);
            Patient patient = new Patient();
            patient.setFirstName(userDto.getFirstName());
            patient.setLastName(userDto.getLastName());
            patient.setUsername(userDto.getUsername());
            patientDAO.save(patient);

            return true;
        }

        return false;
    }

    public void save(User user) {
        userDAO.save(user);
    }

    public User findById(UUID id) {
        return userDAO.findById(id);
    }

    public Set<User> findAll() {
        return userDAO.findAll();
    }

    public User findByUsername(String username) {
        return userDAO.findByUsername(username);
    }

    public void deleteById(UUID id) throws UserNotFoundException {
        userDAO.deleteById(id);
    }
}

