package com.hospital.dto;

import com.hospital.model.Role;
import com.hospital.model.TimeSlot;

import java.util.UUID;

public class NewDoctorDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private Role role = Role.DOCTOR;
    private TimeSlot workTime;

    private NewDoctorDto() {}

    public static DtoBuilder builder() {
        return new DtoBuilder();
    }

    public static class DtoBuilder {
        private NewDoctorDto doctorDto;

        public DtoBuilder() {
            doctorDto = new NewDoctorDto();
        }

        public DtoBuilder setId(UUID id) {
            doctorDto.id = id;
            return this;
        }

        public DtoBuilder setFirstName(String firstName) {
            doctorDto.firstName = firstName;
            return this;
        }

        public DtoBuilder setLastName(String lastName) {
            doctorDto.lastName = lastName;
            return this;
        }

        public DtoBuilder setUsername(String username) {
            doctorDto.username = username;
            return this;
        }

        public DtoBuilder setPassword(String username) {
            doctorDto.password = username;
            return this;
        }

        public DtoBuilder setWorkTime(TimeSlot workTime) {
            doctorDto.workTime = workTime;
            return this;
        }

        public NewDoctorDto build() {
            return doctorDto;
        }

    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public TimeSlot getWorkTime() {
        return workTime;
    }

    public Role getRole() {
        return role;
    }
}
