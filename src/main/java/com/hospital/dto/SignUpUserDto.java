package com.hospital.dto;

import com.hospital.model.Role;

public class SignUpUserDto {
    private String username;
    private String password;
    private Role role = Role.PATIENT;
    private String firstName;
    private String lastName;

    private SignUpUserDto() {}

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public static class UserBuilder {
        private SignUpUserDto signUpUserDto;

        public UserBuilder() {
            signUpUserDto = new SignUpUserDto();
        }

        public UserBuilder setUsername(String username) {
            signUpUserDto.username = username;
            return this;
        }

        public UserBuilder setPassword(String password) {
            signUpUserDto.password = password;
            return this;
        }

        public UserBuilder setRole(Role role) {
            signUpUserDto.role = role;
            return this;
        }

        public UserBuilder setFirstName(String firstName) {
            signUpUserDto.firstName = firstName;
            return this;
        }

        public UserBuilder setLastName(String lastName) {
            signUpUserDto.lastName = lastName;
            return this;
        }

        public SignUpUserDto build() {
            return signUpUserDto;
        }
    }
}
