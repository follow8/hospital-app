package com.hospital.model;



import java.time.Duration;
import java.time.LocalTime;
import java.util.Objects;
import java.util.UUID;

public class TimeSlot {
    private UUID id;
    private Patient patient;
    private UUID doctorId;
    private LocalTime startTime;
    private LocalTime endTime;


    public TimeSlot(){}

    public TimeSlot(LocalTime startTime, LocalTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimeSlot(LocalTime startTime, long duration) {
        this.startTime = startTime;
        this.endTime = startTime.plusMinutes(duration);
    }

    public boolean isTimeSlotCrossing(TimeSlot timeSlot) {
        int startTimeToStartTime = startTime.compareTo(timeSlot.getStartTime());
        int startTimeToEndTime = startTime.compareTo(timeSlot.getEndTime());

        int endTimeToStartTime = endTime.compareTo(timeSlot.getStartTime());
        int endTimeToEndTime = endTime.compareTo(timeSlot.getEndTime());

        if(startTimeToStartTime >= 0 && endTimeToEndTime <= 0) return true;
        if(startTimeToEndTime < 0 && endTimeToEndTime >= 0) return true;
        if(endTimeToStartTime > 0 && startTimeToStartTime <= 0) return true;

        return false;
    }

    public static long durationBetween(LocalTime t1, LocalTime t2) {
        return Duration.between(t1, t2).toMinutes();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public UUID getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(UUID doctorId) {
        this.doctorId = doctorId;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }


    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSlot ts = (TimeSlot) o;
        return Objects.equals(id, ts.id) &&
                Objects.equals(startTime, ts.startTime) &&
                Objects.equals(endTime, ts.endTime) &&
                Objects.equals(patient, ts.patient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime, patient);
    }

    @Override
    public String toString() {
        return "[" + startTime.toString() + " - " + endTime.toString() + "]";
    }
}
