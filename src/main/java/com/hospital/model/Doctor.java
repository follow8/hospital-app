package com.hospital.model;

import java.util.*;

public class Doctor {

    private UUID id;
    private String firstName;
    private String lastName;
    private String username;
    private TimeSlot workTime;
    private SortedSet<TimeSlot> timeSlots;

    public Doctor() {
        this.timeSlots = new TreeSet<>(Comparator.comparing(TimeSlot::getStartTime)
                .thenComparing(TimeSlot::getEndTime));
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public TimeSlot getWorkTime() {
        return workTime;
    }

    public void setWorkTime(TimeSlot workTime) {
        this.workTime = workTime;
    }

    public SortedSet<TimeSlot> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(SortedSet<TimeSlot> timeSlots) {
        this.timeSlots = timeSlots;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(id, doctor.id) &&
                Objects.equals(firstName, doctor.firstName) &&
                Objects.equals(lastName, doctor.lastName) &&
                Objects.equals(username, doctor.username) &&
                Objects.equals(workTime, doctor.workTime) &&
                Objects.equals(timeSlots, doctor.timeSlots);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }
}
