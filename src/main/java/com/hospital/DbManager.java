package com.hospital;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DbManager {
    private static final Logger logger = LogManager.getLogger(DbManager.class);
    private static BasicDataSource ds;
    private static DbManager dbManager;

    public static Connection getConnection()  {
        if(dbManager == null) {
            ds = new BasicDataSource();
            dbManager = new DbManager();
        }
        Connection con = null;
        try{
            con = ds.getConnection();
        }catch (SQLException ex) {
            logger.error(ex.getMessage());
        }

        return con;
    }

     private DbManager() {
        Properties prop = new Properties();
        try (InputStream input = getClass().getClassLoader()
                .getResourceAsStream("./database.properties")) {
            prop.load(input);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }

        String driver = prop.getProperty("driver");
        String url = prop.getProperty("url");
        String username = prop.getProperty("username");
        String password = prop.getProperty("password");

        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);

    }
}
