package com.hospital.servlets.doctorservlets;

import com.hospital.model.Doctor;
import com.hospital.services.DoctorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/doctor")
public class HomeServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(HomeServlet.class);
    private DoctorService doctorService = new DoctorService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Processing GET request in: " + getClass().getSimpleName());
        HttpSession session = req.getSession(false);
        if (session != null) {
            String username = ((String) session.getAttribute("username"));
            Doctor doc = doctorService.findByUsername(username);
            req.setAttribute("doctor", doc);
        }

        req.getRequestDispatcher("/jsp/doctor/doctorSchedule.jsp").forward(req, resp);
    }
}
