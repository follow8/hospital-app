package com.hospital.servlets.doctorservlets;

import com.hospital.model.Doctor;
import com.hospital.services.DoctorService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/doctor/doctorSchedule")
public class DoctorScheduleServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UUID doctorId = UUID.fromString(req.getParameter("doctorId"));
        Doctor doc = doctorService.findById(doctorId);
        req.setAttribute("doctor", doc);
        req.getRequestDispatcher("/jsp/doctor/doctorSchedule.jsp").forward(req, resp);
    }
}
