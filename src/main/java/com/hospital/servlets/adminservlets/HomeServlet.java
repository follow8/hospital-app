package com.hospital.servlets.adminservlets;

import com.hospital.model.Doctor;
import com.hospital.services.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet("/admin")
public class HomeServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Set<Doctor> doctorSet = doctorService.findAll();
        req.setAttribute("doctorList", doctorSet);
        req.getRequestDispatcher("/jsp/admin/home.jsp").forward(req, resp);
    }
}
