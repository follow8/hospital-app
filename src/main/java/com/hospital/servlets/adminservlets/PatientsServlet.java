package com.hospital.servlets.adminservlets;

import com.hospital.model.Patient;
import com.hospital.services.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet("/admin/patients")
public class PatientsServlet extends HttpServlet {
    private PatientService patientService = new PatientService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Set<Patient> patientSet = patientService.findAll();
        req.setAttribute("patientList", patientSet);
        req.getRequestDispatcher("/jsp/admin/patients.jsp").forward(req, resp);
    }
}
