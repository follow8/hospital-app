package com.hospital.servlets.adminservlets;

import com.hospital.exceptions.DoctorNotFoundException;
import com.hospital.services.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/admin/deleteDoctor")
public class DeleteDoctorServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UUID doctorId = UUID.fromString(req.getParameter("doctorId"));
        try {
            doctorService.deleteById(doctorId);
        } catch (DoctorNotFoundException ex) {
            req.setAttribute("errorMessage", "Doctor not found");
        }
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/admin"));
    }
}
