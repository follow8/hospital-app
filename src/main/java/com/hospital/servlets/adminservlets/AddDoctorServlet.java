package com.hospital.servlets.adminservlets;

import com.hospital.dto.NewDoctorDto;
import com.hospital.model.TimeSlot;
import com.hospital.services.DoctorService;
import com.hospital.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;

@WebServlet("/admin/doctorAdd")
public class AddDoctorServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    private UserService userService = new UserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        LocalTime startTime = LocalTime.parse(req.getParameter("startTime"));
        LocalTime endTime = LocalTime.parse(req.getParameter("endTime"));
        if(startTime.compareTo(endTime) >= 0) {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/admin/profile"));
        }
        TimeSlot ts = new TimeSlot(startTime, endTime);
        NewDoctorDto doctorDto = NewDoctorDto.builder()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setPassword(password)
                .setUsername(username)
                .setWorkTime(ts)
                .build();

        if(userService.findByUsername(username) == null) {
            doctorService.save(doctorDto);
        }else {
           req.setAttribute("errorMessage", "Such username already exists!");
        }
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/admin/profile"));
    }
}
