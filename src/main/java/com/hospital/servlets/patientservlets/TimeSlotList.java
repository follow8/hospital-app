package com.hospital.servlets.patientservlets;

import com.hospital.model.Patient;
import com.hospital.model.TimeSlot;
import com.hospital.services.PatientService;
import com.hospital.services.TimeSlotService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

@WebServlet("/patient/timeSlots")
public class TimeSlotList extends HttpServlet {
    private PatientService patientService = new PatientService();
    private TimeSlotService timeSlotService = new TimeSlotService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session != null) {
            String username = ((String) session.getAttribute("username"));
            Patient patient = patientService.findByUsername(username);
            if(patient != null) {
                Set<TimeSlot> timeSlots = timeSlotService.findByPatientId(patient.getId());
                req.setAttribute("timeSlotList", timeSlots);
            }
        }

        req.getRequestDispatcher("/jsp/patient/timeSlots.jsp").forward(req, resp);
    }
}
