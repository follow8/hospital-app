package com.hospital.servlets.patientservlets;

import com.hospital.exceptions.TimeSlotIsBusyException;
import com.hospital.model.Doctor;
import com.hospital.model.Patient;
import com.hospital.model.TimeSlot;
import com.hospital.model.User;
import com.hospital.services.DoctorService;
import com.hospital.services.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;
import java.util.SortedSet;
import java.util.UUID;

@WebServlet("/patient/bookTimeSlot")
public class BookTimeSlotServlet extends HttpServlet {
    private DoctorService doctorService = new DoctorService();
    private PatientService patientService = new PatientService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UUID id = UUID.fromString(req.getParameter("doctorId"));
        Doctor doc = doctorService.findById(id);
        SortedSet<TimeSlot> timeSlots = doctorService.getAllAvailableTimeSlots(doc);
        req.setAttribute("doctor", doc);
        req.setAttribute("timeSlotList", timeSlots);
        req.getRequestDispatcher("/jsp/patient/bookTimeSlot.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UUID doctorId = UUID.fromString(req.getParameter("doctorId"));
        LocalTime startTime = LocalTime.parse(req.getParameter("startTime"));
        LocalTime endTime = LocalTime.parse(req.getParameter("endTime"));
        String username = ((String) req.getSession().getAttribute("username"));
        Patient patient = patientService.findByUsername(username);
        Doctor doc = doctorService.findById(doctorId);
        SortedSet<TimeSlot> availableTimeSlots = doctorService.getAllAvailableTimeSlots(doc);
        if(startTime.compareTo(endTime) >= 0) {
            req.setAttribute("incorrectTime", "Incorrect time!");
            req.setAttribute("timeSlotList", availableTimeSlots);
            req.getRequestDispatcher("/jsp/patient/bookTimeSlot.jsp").forward(req, resp);
        }
        if (doc != null) {
            TimeSlot ts = new TimeSlot(startTime, endTime);
            ts.setPatient(patient);
            ts.setDoctorId(doctorId);
            try {
                doctorService.addNewTimeSlot(doc, ts);
                availableTimeSlots = doctorService.getAllAvailableTimeSlots(doc);
            } catch (TimeSlotIsBusyException ex) {
                req.setAttribute("timeSlotIsBusyMessage", "Time slot is busy!");
                availableTimeSlots = doctorService.getAllNearestAvailableTimeSlots(doc, startTime);
            }
        }
        req.setAttribute("timeSlotList", availableTimeSlots);
        req.setAttribute("doctor", doc);
        req.getRequestDispatcher("/jsp/patient/bookTimeSlot.jsp").forward(req, resp);
    }
}
