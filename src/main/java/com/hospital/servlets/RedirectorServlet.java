package com.hospital.servlets;

import com.hospital.model.User;
import com.hospital.services.UserService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/redirect")
public class RedirectorServlet extends HttpServlet {
    private UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            String username = ((String) session.getAttribute("username"));
            User user = userService.findByUsername(username);
            if (user != null) {
                switch (user.getRole()) {
                    case ADMIN:
                        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/admin"));
                        break;
                    case DOCTOR:
                        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/doctor"));
                        break;
                    case PATIENT:
                        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/patient"));
                        break;
                    default:
                        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/authorization"));
                        break;
                }
            } else {
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/authorization"));
            }
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/authorization"));
        }
    }
}
