package com.hospital.servlets;

import com.hospital.dto.SignInUserDto;
import com.hospital.model.User;
import com.hospital.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/authorization")
public class AuthorizationServlet extends HttpServlet {
    private final UserService userService;
    private static final Logger logger = LogManager.getLogger(AuthorizationServlet.class);

    public AuthorizationServlet() {
        userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET request in " + getServletName());
        req.getRequestDispatcher("/jsp/authorization.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST request in " + getServletName());
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if(username != null && password != null
                && username.isEmpty() == false && password.isEmpty() == false) {
            SignInUserDto signInUserDto = new SignInUserDto(username, password);
            if (userService.signIn(signInUserDto)) {
                HttpSession httpSession = req.getSession();
                httpSession.setAttribute("username", username);
                httpSession.setMaxInactiveInterval(600);
                resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/redirect"));
            } else {
                req.setAttribute("errorMessage", "Login failed!");
                req.getRequestDispatcher("/jsp/authorization.jsp").forward(req, resp);
            }
        }else {
            req.setAttribute("errorMessage", "Login failed!");
            req.getRequestDispatcher("/jsp/authorization.jsp").forward(req, resp);
        }
    }
}
