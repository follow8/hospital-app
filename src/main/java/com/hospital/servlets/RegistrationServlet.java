package com.hospital.servlets;

import com.hospital.dto.SignUpUserDto;
import com.hospital.model.User;
import com.hospital.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(RegistrationServlet.class);
    private final UserService userService;

    public RegistrationServlet() {
        userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("GET request in " + getServletName());
        req.getRequestDispatcher("/jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("POST request in " + getServletName());
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");

        SignUpUserDto signUpUserDto = SignUpUserDto.builder()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setUsername(username)
                .setPassword(password)
                .build();

        if (userService.singUp(signUpUserDto)) {
            HttpSession session = req.getSession();
            session.setAttribute("username", username);
            session.setMaxInactiveInterval(600);
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/redirect"));
        } else {
            req.setAttribute("errorMessage", "Username already exists!");
            req.getRequestDispatcher("/jsp/registration.jsp").forward(req, resp);
        }
    }
}

